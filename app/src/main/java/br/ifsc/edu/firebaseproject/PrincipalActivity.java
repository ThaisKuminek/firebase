package br.ifsc.edu.firebaseproject;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

class PrincipalActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    protected void onCreate (Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth=FirebaseAuth.getInstance();

    }
    public void logout (View view){
        
        mAuth.signOut();
        finish();
    }

}
