package br.ifsc.edu.firebaseproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mAuth=FirebaseAuth.getInstance().getInstance();
        mAuth.createUserWithEmailAndPassword("thaiseduarda2222@gmail.com", "123bla").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    Toast.makeText(getApplicationContext(), mAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), PrincipalActivity.class);
            }else{
                Toast.makeText(getApplicationContext(), "falha no login", Toast.LENGTH_LONG).show();
            }


        }});
       // mAuth.signInWithEmailAndPassword("")

        FirebaseUser firebaseUser=mAuth.getCurrentUser();
            if(firebaseUser != null)

    {
        Log.d("FirebaseUserexemplo", "Usuário logado" + firebaseUser.getEmail());
    }else

    {
        Log.d("FirebaseUserExemplo", "Falha na autenticação");


    }
    }

    public void logout(View view) {
    }
}
